const std = @import("std");
const curl = @cImport({
    @cInclude("curl/curl.h");
});

const Story = struct {
    title: []u8,
    url: []u8,
    by: []u8,
    score: i32,
};

const stdout = std.io.getStdOut().writer();

pub fn main() anyerror!void {
    var arena_state = std.heap.ArenaAllocator.init(std.heap.c_allocator);
    defer arena_state.deinit();

    const alloc = arena_state.allocator();

    const handle = curl.curl_easy_init() orelse return error.CURLHandleInitFailed;
    defer curl.curl_easy_cleanup(handle);

    var story_ids = std.ArrayList(u8).init(alloc);
    defer story_ids.deinit();

    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_URL, "https://hacker-news.firebaseio.com/v0/topstories.json");
    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_FOLLOWLOCATION, true);
    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_WRITEFUNCTION, writeFunc);
    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_WRITEDATA, &story_ids);
    _ = curl.curl_easy_perform(handle);

    // parsing items
    var stream = std.json.TokenStream.init(story_ids.items);
    const parsed_ids = try std.json.parse([]u32, &stream, .{ .allocator = alloc });
    defer std.json.parseFree([]u32, parsed_ids, .{ .allocator = alloc });

    var threads = std.ArrayList(std.Thread).init(alloc);
    defer threads.deinit();

    var top_five = parsed_ids[0..5];
    for (top_five) |id| {
        const thread = try std.Thread.spawn(std.Thread.SpawnConfig{}, fetchStory, .{ alloc, id });
        try threads.append(thread);
    }

    for (threads.items) |thread| {
        thread.join();
    }
}

fn fetchStory(alloc: std.mem.Allocator, id: u32) !void {
    var story_buf = std.ArrayList(u8).init(alloc);
    defer story_buf.deinit();

    const story_url = try std.fmt.allocPrintZ(alloc, "https://hacker-news.firebaseio.com/v0/item/{d}.json", .{id});
    defer alloc.free(story_url);

    var handle = curl.curl_easy_init() orelse return error.CURLHandleInitFailed;
    defer curl.curl_easy_cleanup(handle);
    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_URL, story_url.ptr);
    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_WRITEFUNCTION, writeFunc);
    _ = curl.curl_easy_setopt(handle, curl.CURLOPT_WRITEDATA, &story_buf);
    _ = curl.curl_easy_perform(handle);

    var story_json_stream = std.json.TokenStream.init(story_buf.items);
    var story = try std.json.parse(Story, &story_json_stream, .{ .allocator = alloc, .ignore_unknown_fields = true });
    defer std.json.parseFree(Story, story, .{ .allocator = alloc });

    try stdout.print("{s}\nScore: {d}\nBy: {s}\nURL: {s}\n\n", .{ story.title, story.score, story.by, story.url });
}

fn writeFunc(data: *anyopaque, size: c_uint, nmemb: c_uint, user_data: *anyopaque) callconv(.C) c_uint {
    var buffer = @intToPtr(*std.ArrayList(u8), @ptrToInt(user_data));
    var typed_data = @intToPtr([*]u8, @ptrToInt(data));
    buffer.appendSlice(typed_data[0 .. nmemb * size]) catch return 0;
    return nmemb * size;
}
